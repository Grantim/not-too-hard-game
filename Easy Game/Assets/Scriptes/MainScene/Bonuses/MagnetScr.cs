﻿using UnityEngine;

public class MagnetScr : MonoBehaviour {
	int k;
	public bool magneting;
	GameObject[] coins, bonuses;
	Vector2 r;
	// Use this for initialization
	void Start () {
		magneting = false;
		GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);	
		k = 0;	
	}
	public void StopMagneting(){
		coins = GameObject.FindGameObjectsWithTag("Coin");
		bonuses = GameObject.FindGameObjectsWithTag ("Bonus");
		GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);	
		foreach (GameObject coin in coins) {
			coin.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}
		foreach (GameObject bonus in bonuses) {
			bonus.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}
	}
	void Magneting(){
		coins = GameObject.FindGameObjectsWithTag("Coin");
		bonuses = GameObject.FindGameObjectsWithTag ("Bonus");
		foreach (GameObject coin in coins) {
			r = transform.position - coin.transform.position;
			coin.GetComponent<Rigidbody2D>().velocity = new Vector2((10f*r.x)/(r.x*r.x+r.y*r.y),(10f*r.y)/(r.x*r.x+r.y*r.y));
		}
		foreach (GameObject bonus in bonuses) {
			r = transform.position - bonus.transform.position;
			bonus.GetComponent<Rigidbody2D>().velocity = new Vector2((13f*r.x)/(r.x*r.x+r.y*r.y),(13f*r.y)/(r.x*r.x+r.y*r.y));
		}
	}
	// Update is called once per frame
	void Update () {
		k = GameObject.Find ("Center").GetComponent<CenterScr> ().multy;
		transform.position =new Vector3 (GameObject.Find ("Center").transform.position.x,
		                                 GameObject.Find ("Center").transform.position.y,
		                                 GameObject.Find ("Center").transform.position.z+0.1f);
		if (magneting)
			Magneting ();	
		if (k > 10)
			transform.localScale = new Vector2 (0.1f*k+1f,0.1f*k+1f);
		else
			transform.localScale = new Vector2 (2f,2f);
	}
}
