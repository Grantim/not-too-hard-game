﻿using UnityEngine;

public class SheildStartScr : MonoBehaviour {
	public GameObject sexyScore;
	float existanceTimer;
	float clr = 1f;
	void Start(){
		existanceTimer = 0f;
	}
	void DoWhatYouWasBornToDo(Collider2D coll){
		bool idCheck = false;
		if (coll.gameObject.tag == "Circle")
			if (coll.gameObject.GetComponent<FellowScr> ().id != -1)
				idCheck = true;
		if (coll.gameObject.tag == "Magnet")
			if (coll.gameObject.GetComponent<MagnetScr> ().magneting)
				idCheck = true;
		if (coll.gameObject.tag == "Player")
			idCheck = true;
		if (idCheck && GameObject.Find("Center").GetComponent<CenterScr> ().alive) {
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                GameObject.Find("Center").GetComponent<AudioSource>().clip =
                  GameObject.Find("Center").GetComponent<CenterScr>().auClip[0];
                GameObject.Find("Center").GetComponent<AudioSource>().Play();
            }
            GameObject.Find ("Center").GetComponent<CenterScr> ().sheilded = true;
			GameObject.Find ("Center").GetComponent<CenterScr> ().sheildTimer = 0f;
			sexyScore.GetComponent<GUIText>().fontSize = 30;
			sexyScore.GetComponent<GUIText>().color = Color.blue;
			sexyScore.GetComponent<GUIText>().text = "shield";
			sexyScore.transform.position = (coll.gameObject.transform.position);
			Instantiate(sexyScore);
			Destroy(gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D coll){
		DoWhatYouWasBornToDo (coll);
	}
	/*void OnTriggerStay2D(Collider2D coll){
		DoWhatYouWasBornToDo (coll);
	}*/
	void Update () {
		existanceTimer += Time.deltaTime;
		if (existanceTimer >= 1f) {			
			clr -= 0.02f;
			if (GetComponent<SpriteRenderer> ().color.a > 0)
				GetComponent<SpriteRenderer> ().color = new Color(1,1,1,clr);
			else
				Destroy (gameObject);            
		}	
	}
}
