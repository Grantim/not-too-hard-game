﻿using UnityEngine;

public class SheildScr : MonoBehaviour {
	int k;
	// Use this for initialization
	/*
	 * 
	 * 
	 * Look for etalonTimer in CenterScr
	 */
	void Start () {
		GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
		k = 0;	
	}
	void OnTriggerEnter2D(Collider2D coll){
        if (coll.gameObject.tag == "BadCircle" &&
            GameObject.Find("Center").GetComponent<CenterScr>().alive &&
            GameObject.Find("Center").GetComponent<CenterScr>().sheilded)
        {
            coll.GetComponent<BadOneScr>().readyToDie = true;
            if (PlayerPrefs.GetInt("sound", 1) == 1)
                GetComponent<AudioSource>().Play();
        }
	}
	void OnTriggerStay2D(Collider2D coll){
		if (coll.gameObject.tag == "BadCircle" && 
		    GameObject.Find ("Center").GetComponent<CenterScr>().alive &&
		    GameObject.Find ("Center").GetComponent<CenterScr>().sheilded)
			coll.GetComponent<BadOneScr> ().readyToDie = true;		
	}
	// Update is called once per frame
	void Update () {
		k = GameObject.Find ("Center").GetComponent<CenterScr> ().multy;
		transform.position =new Vector3 (GameObject.Find ("Center").transform.position.x,
		                                 GameObject.Find ("Center").transform.position.y,
		                                 GameObject.Find ("Center").transform.position.z+0.1f);	
		if (k > 10)
			transform.localScale = new Vector2 (0.1f*k+0.3f,0.1f*k+0.3f);
		else
			transform.localScale = new Vector2 (1.3f,1.3f);
	}
}
