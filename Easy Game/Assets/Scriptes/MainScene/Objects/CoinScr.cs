﻿using UnityEngine;

public class CoinScr : MonoBehaviour {
    float existanceTimer;
	float clr = 1f;
	public GameObject sexyScore;
	// Use this for initialization
	void Start () {
        existanceTimer = 0f;	
	}
	void DoWhatYouWasBornToDo(Collider2D coll){
		bool idCheck = false;
		if (coll.gameObject.tag == "Circle")
			if (coll.gameObject.GetComponent<FellowScr> ().id != -1)
				idCheck = true;
		if (coll.gameObject.tag == "Magnet")
			if (coll.gameObject.GetComponent<MagnetScr> ().magneting)
				idCheck = true;
		if (coll.gameObject.tag == "Player")
			idCheck = true;
		if (idCheck && GameObject.Find("Center").GetComponent<CenterScr> ().alive) {
            if (PlayerPrefs.GetInt("sound", 1) == 1)
            {
                GameObject.Find("Center").GetComponent<AudioSource>().clip =
                  GameObject.Find("Center").GetComponent<CenterScr>().auClip[0];
                GameObject.Find("Center").GetComponent<AudioSource>().Play();
            }
            sexyScore.GetComponent<GUIText>().fontSize = 35;
			sexyScore.GetComponent<GUIText>().color = GameObject.Find("Money").GetComponent<MoneyTextScr>().color;
			sexyScore.GetComponent<GUIText>().text = "+"+GameObject.Find("Money").GetComponent<MoneyTextScr>().scale.ToString()+"$";
			sexyScore.transform.position = (coll.gameObject.transform.position);
			Instantiate(sexyScore);
			GameObject.Find("Money").GetComponent<MoneyTextScr>().gotCoin = true;
			Destroy(gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D coll){
		DoWhatYouWasBornToDo (coll);
	}
	/*void OnTriggerStay2D(Collider2D coll){
		DoWhatYouWasBornToDo (coll);
	}*/
	// Update is called once per frame
	void Update () {
        existanceTimer += Time.deltaTime;
        if (existanceTimer >= 2f) {			
			clr -= 0.01f;
			if (GetComponent<SpriteRenderer> ().color.a > 0)
				GetComponent<SpriteRenderer> ().color = new Color(1,1,1,clr);
			else
				Destroy (gameObject);            
        }
	}
}
