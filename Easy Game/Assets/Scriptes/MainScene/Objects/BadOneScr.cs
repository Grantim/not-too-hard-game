﻿using UnityEngine;

public class BadOneScr : MonoBehaviour {
	Rigidbody2D rg;
	public GameObject sexyScore;
	public bool readyToDie;
	GameObject center;
	Vector3 screen;
	// Use this for initialization
	void Start () {
		readyToDie = false;
		center = GameObject.Find ("Center");
		rg = GetComponent<Rigidbody2D>();
		float ax, ay;
		ax = (Random.value * 26f) - 13f;
		ay = (Random.value * 40f) - 20f;
		float v = Random.value * 2f + 3f;
		rg.velocity = new Vector2 ((ax - transform.position.x)/v,(ay-transform.position.y)/v);
		rg.angularVelocity = Random.value * 70f - 35f;
	}
	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "Circle") {
			if (coll.gameObject.GetComponent<FellowScr> ().dtn) {
				//Camera.main.WorldToViewportPoint(coll.transform.position);
				screen = new Vector3 (coll.gameObject.transform.position.x, coll.gameObject.transform.position.y);
				sexyScore.transform.position = new Vector3 (screen.x, screen.y, 7f);			
				if (GameObject.Find ("Center").GetComponent<CenterScr> ().multy != 0) {
					GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy;
					sexyScore.GetComponent<GUIText> ().text = (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy).ToString ();
					if (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy >= 1500) {
						sexyScore.GetComponent<GUIText> ().fontSize = 42;
						sexyScore.GetComponent<GUIText> ().color = Color.white;
					} else {
						sexyScore.GetComponent<GUIText> ().fontSize = 30;
						sexyScore.GetComponent<GUIText> ().color = Color.white;
					}
					if (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy >= 2500)
						sexyScore.GetComponent<GUIText> ().color = Color.red;
				} else {
					GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 250;
					sexyScore.GetComponent<GUIText> ().color = Color.white;
					sexyScore.GetComponent<GUIText> ().fontSize = 30;
					sexyScore.GetComponent<GUIText> ().text = "250";
				}
				Instantiate (sexyScore);
			}
			if (!readyToDie)//(!GameObject.Find ("Center").GetComponent<CenterScr> ().sheilded || coll.gameObject.GetComponent<FellowScr>().id == -1)
				coll.gameObject.GetComponent<FellowScr> ().Die ();
		}
	}
    void Die(){            
		if (transform.localScale.x <= 0.1) {
			Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
			sexyScore.GetComponent<GUIText>().fontSize = 30;
			sexyScore.GetComponent<GUIText>().color = Color.blue;
			if (GameObject.Find("Center").GetComponent<CenterScr>().alive && GameObject.Find("Center").GetComponent<CenterScr>().sheilded)
            {			
				if (GameObject.Find("Center").GetComponent<CenterScr>().multy != 0){
					GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 100*GameObject.Find("Center").GetComponent<CenterScr>().multy;
					sexyScore.GetComponent<GUIText>().text = (100*GameObject.Find("Center").GetComponent<CenterScr>().multy).ToString();
				} else {
					GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 50;
					sexyScore.GetComponent<GUIText>().text = (50).ToString();
				}
			}
			if (pos.x >= 1f)
				pos.x = 0.85f;
			else if (pos.x <= 0f)
				pos.x = 0.1f;
			if (pos.y >= 1f)
				pos.y = 0.85f;
			else if (pos.y <= 0f)
				pos.y = 0.1f;
			if (GameObject.Find("Center").GetComponent<CenterScr>().sheilded){
				sexyScore.transform.position = Camera.main.ViewportToWorldPoint(pos);
				Instantiate(sexyScore);
			}
			Destroy (gameObject);
		} else
			transform.localScale = transform.localScale - (new Vector3 (0.05f, 0.05f, 0f));

	}
	// Update is called once per frame
	void Update () {
		if (Mathf.Sqrt ((transform.position.x * transform.position.x) + (transform.position.y * transform.position.y)) > 20f)
			Destroy (gameObject);
		if (!readyToDie && center.GetComponent<CenterScr> ().alive && center.GetComponent<CenterScr> ().prs &&
		    Mathf.Sqrt ((center.transform.position.x - transform.position.x) * (center.transform.position.x - transform.position.x) +
			(center.transform.position.y - transform.position.y) * (center.transform.position.y - transform.position.y)) <= 2.465f) {
			center.GetComponent<SpriteRenderer>().color = Color.red;
			center.GetComponent<CenterScr> ().alive = false;
			center.GetComponent<CenterScr> ().Die();
			GameObject.Find("Filter").GetComponent<FilterScr>().Filter();
			center.GetComponent<Rigidbody2D>().drag = 2f;
		}
		if (readyToDie || !GameObject.Find("Center").GetComponent<CenterScr>().alive || transform.localScale.x < 1f)
			Die ();
	}
}
