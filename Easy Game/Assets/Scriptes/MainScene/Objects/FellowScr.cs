﻿using UnityEngine;

public class FellowScr : MonoBehaviour {
	Vector3 pos,mpos,posc;
	Vector2 force,ownforce;
	GameObject center;
	public float RO;
	public GameObject sexyScore;
	//float frc;
	Rigidbody2D rg;
	bool prs = false,sound = true;
	public bool dtn = false;
	public int id = -1;
	float rx,ry,timer,timer2,dtnChance;
	// Use this for initialization
	void Start () {
		dtnChance = 1f;
		center = GameObject.Find("Center");
		timer2 = 0f;
		timer = 0f;
		if (Input.GetMouseButton(0)) 
			prs = true;
		pos = transform.position;
		mpos = Input.mousePosition;
		rg = GetComponent<Rigidbody2D>();
		//Random.Range (-40,40);
		float ax, ay;
		ax = (Random.value * 8f) - 4f ;
		//Random.Range (-50, 50);
		ay = (Random.value * 10f) - 5f;
		rg.velocity = new Vector2 (ax - transform.position.x/5f,ay-transform.position.y/5f);
		ownforce = rg.velocity;
		rx = -(pos.x - mpos.x);
		ry = -(pos.y - mpos.y);
	}
	
	// Update is called once per frame
	/*void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Circle")
		col.gameObject.GetComponent<Rigidbody2D> ().velocity += rg.velocity;
	}*/
	public void Die(){
        if (sound &&  id!=-1 && GameObject.Find("Center").GetComponent<CenterScr>().alive && PlayerPrefs.GetInt("sound", 1) == 1) {
            GameObject.Find("Center").GetComponent<AudioSource>().clip =
                GameObject.Find("Center").GetComponent<CenterScr>().auClip[2];
            GameObject.Find("Center").GetComponent<AudioSource>().Play();
        }
		GameObject.Find ("Center").GetComponent<CenterScr> ().EVENT1 = true;
		GameObject.Find("Spawner").GetComponent<SpawnerScr>().n--;
		Destroy (gameObject);
		//GameObject.Find("Spawner").GetComponent<SpawnerScr>().freeid = id;
	}


	void Detonation(){
        sound = false;
        if (GameObject.Find("Center").GetComponent<CenterScr>().alive && PlayerPrefs.GetInt("sound", 1) == 1)
        {
            GameObject.Find("Magnet").GetComponent<AudioSource>().Play();
        }
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Circle")) {				
			Vector2 n = new Vector2 (go.transform.position.x - transform.position.x, go.transform.position.y - transform.position.y);				
			if (go.GetComponent<FellowScr> ().id != id || go.GetComponent<FellowScr> ().id==-1){
				if(!float.IsNaN(go.GetComponent<Rigidbody2D>().velocity.x+15f * n.x / (Mathf.Sqrt (n.x * n.x + n.y * n.y)))&&!float.IsNaN(go.GetComponent<Rigidbody2D>().velocity.y+ 15f * n.y / (Mathf.Sqrt (n.x * n.x + n.y * n.y))))
				go.GetComponent<Rigidbody2D> ().velocity += new Vector2 (15f * n.x / (Mathf.Sqrt (n.x * n.x + n.y * n.y)), 15f * n.y / (Mathf.Sqrt (n.x * n.x + n.y * n.y)));
				if (Mathf.Sqrt((go.transform.position.x-transform.position.x)*(go.transform.position.x-transform.position.x)+(go.transform.position.y-transform.position.y)*(go.transform.position.y-transform.position.y)) < 2f)
					go.GetComponent<FellowScr>().Die();
			}
		}			
		if (prs && center.GetComponent<CenterScr> ().alive &&
		    Mathf.Sqrt ((transform.position.x - center.transform.position.x) * (transform.position.x - center.transform.position.x) + 
		            (transform.position.y - center.transform.position.y) * (transform.position.y - center.transform.position.y)) < 2f) {				
			center.GetComponent<CenterScr> ().alive = false;				
			GameObject.Find("Filter").GetComponent<FilterScr>().Filter();
			GameObject.Find("Center").GetComponent<CenterScr>().Die();
			GameObject.Find("Center").GetComponent<SpriteRenderer>().color = Color.red;
			GameObject.Find("Center").GetComponent<Rigidbody2D>().drag = 2f;
		}
		Die ();			
	}
	void MPOS(){
		int n = GameObject.Find ("Center").GetComponent<CenterScr> ().multy;
		//Debug.Log (n);
		if (n != 0){
			float omega = GameObject.Find ("Center").GetComponent<CenterScr> ().omega;
			int k;
			if (n > 10)
				k = n;
			else k = 10;
			mpos.x += (k * 0.1f) * Mathf.Cos (omega + 2 * Mathf.PI * (id) / (n));  
			mpos.y += (k * 0.1f) * Mathf.Sin (omega + 2 * Mathf.PI * (id) / (n));
		}
	
	}
	void Update () {
		timer2 += Time.deltaTime;
		timer += Time.deltaTime;
		dtnChance = 1f + (GameObject.Find ("Text").GetComponent<ScoreScr> ().score / 500) * 0.005f;
		if (timer >= 0.5f  && !dtn) {
			if (Random.value*100f < dtnChance && !center.GetComponent<CenterScr>().sheilded && id != -1){
				dtn = true;
				transform.position = new Vector3(transform.position.x,transform.position.y,-0.5f);
				GetComponent<Animator>().SetBool("DTN",true);
			}
			timer2 = 0;
			timer = 0;
		}
		if (timer2 > 2f)
			Detonation ();
		if (dtn && center.GetComponent<CenterScr> ().sheilded) {
			if (GameObject.Find ("Center").GetComponent<CenterScr> ().multy != 0) {
				GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy;
				sexyScore.GetComponent<GUIText> ().text = (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy).ToString ();
				if (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy >= 1500) {
					sexyScore.GetComponent<GUIText> ().fontSize = 42;
					sexyScore.GetComponent<GUIText> ().color = Color.blue;
				} else {
					sexyScore.GetComponent<GUIText> ().fontSize = 30;
					sexyScore.GetComponent<GUIText> ().color = Color.blue;
				}
				if (250 * GameObject.Find ("Center").GetComponent<CenterScr> ().multy >= 2500)
					sexyScore.GetComponent<GUIText> ().color = Color.red;
			} else {
				GameObject.Find ("Text").GetComponent<ScoreScr> ().score += 250;
				sexyScore.GetComponent<GUIText> ().color = Color.blue;
				sexyScore.GetComponent<GUIText> ().fontSize = 30;
				sexyScore.GetComponent<GUIText> ().text = "250";
			}
			sexyScore.transform.position = new Vector3(transform.position.x,transform.position.y-1.5f,7f);
			Instantiate (sexyScore);
			Die ();
		}
		mpos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		RO = Mathf.Sqrt ((pos.x - GameObject.Find("Center").transform.position.x) * (pos.x - GameObject.Find("Center").transform.position.x) + (pos.y - GameObject.Find("Center").transform.position.y) * (pos.y - GameObject.Find("Center").transform.position.y));
		posc = mpos;
		//MPOS ();
		pos = transform.position;
		rx = -(pos.x - mpos.x);
		ry = -(pos.y - mpos.y);
		if (RO <= 3f && id==-1)
			GameObject.Find ("Center").GetComponent<CenterScr> ().EVENT1 = true;
		if (RO > 3f && id != -1) {
			GameObject.Find ("Center").GetComponent<CenterScr> ().EVENT1 = true;
			id = -1;
		}
		if (RO > 25f) {
			Die ();
		}
		if (/*(Mathf.Sqrt(rx*rx+ry*ry) > 0.5f) &&*/ prs && center.GetComponent<CenterScr>().alive){		
			//frc = 0.1f/(rx*rx+ry*ry);
			if (RO<= 2f){
				MPOS ();//
				pos = transform.position;
				rx = -(pos.x - mpos.x);
				ry = -(pos.y - mpos.y);
				force.x = 2f*(rx);			
				force.y = 2f*(ry);	
				ownforce = force;
			}
			else {
				pos = transform.position;
				rx = -(pos.x - mpos.x);
				ry = -(pos.y - mpos.y);
				force.x =0.98f*ownforce.x + (0.08f*rx);			
				force.y =0.98f*ownforce.y + (0.08f*ry);
				//Debug.Log(ownforce);
				ownforce = force;
			}
			//Debug.Log(ownforce);
			if (!float.IsNaN(ownforce.x)&&!float.IsNaN(ownforce.y))
			rg.velocity = ownforce;
		}
		if (Input.GetMouseButtonDown (0)) {
			prs = true;
			/*if (center.GetComponent<CenterScr>().deathTimer <= 0f){
				center.GetComponent<CenterScr>().alive = true;
				center.GetComponent<CenterScr>().deathTimer = 1.5f;
			}*/
		}
		if (Input.GetMouseButtonUp (0)) {
			if (center.GetComponent<CenterScr>().alive){
				Vector2 k = new Vector2(transform.position.x - posc.x,transform.position.y-posc.y);
				rg.velocity += (new Vector2(8f*k.x/Mathf.Sqrt(k.x*k.x+k.y*k.y),8f*k.y/Mathf.Sqrt(k.x*k.x+k.y*k.y)));
			}
			//if (!float.IsNaN(ownforce.x)&&!float.IsNaN(ownforce.y))
			ownforce = rg.velocity;
			prs = false;
		}
	}
}
