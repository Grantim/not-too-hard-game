﻿using UnityEngine;

public class BonusSpawnerScr : MonoBehaviour {
	public GameObject CoinScale, Magnet, Shield;
	public float spawnChance;
	float timer,posTimer,setBonus;
	// Use this for initialization
	void Start () {
		spawnChance = 10f + 4f*PlayerPrefs.GetInt("dropLvl",0);	
	}
	
	// Update is called once per frame
	void Update () {		
		posTimer += Time.deltaTime;	
		timer += Time.deltaTime;
		if (timer > 2f) {
			timer = 0f;
			if (Random.value*100f < spawnChance){
				float rand = Random.value;
				if (rand*3f <= 1f){
					CoinScale.transform.position = new Vector3(transform.position.x,transform.position.y,1f); 
					Instantiate(CoinScale);
				} else if (rand*3f<=2f) {
					Shield.transform.position = new Vector3(transform.position.x,transform.position.y,1f); 
					Instantiate(Shield);
				} else {
					Magnet.transform.position = new Vector3(transform.position.x,transform.position.y,1f); 
					Instantiate(Magnet);
				}
			}
		}	
		if (posTimer >= 0.5f) {
			posTimer = 0f;
			transform.position = new Vector3(Random.value*12f-6f,Random.value*8f-4f,8);
		}
	}
}
