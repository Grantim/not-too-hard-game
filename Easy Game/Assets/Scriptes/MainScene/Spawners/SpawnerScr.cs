﻿using UnityEngine;

public class SpawnerScr : MonoBehaviour {
	Vector3 pos;
	public int n,nmax;
	float k;
	//int i;
	//public int freeid;
	float f,d,dBad,deadTimer;
	bool spawned = false, spawnedBad = false;
	Vector2 startvel;
	public GameObject go,goBad;
	GameObject[] gos;
	// Use this for initialization
	void Start () {
		deadTimer = 0f;
		k = 0f;
		n = 0;
		nmax = 20;
		dBad = 0f;
		f = 0f;
		d = 0f;
		pos = transform.position;	
	}
	
	// Update is called once per frame
	void Update () {
		dBad += Time.deltaTime;
		f += Time.deltaTime;
		d += Time.deltaTime;
		pos.y = 12f * Mathf.Sin (f);
		pos.x = 12f * Mathf.Cos (f);
		transform.position = pos;
		if (!spawnedBad && (Random.value * 1000f < 15f)) {
			goBad.transform.position = new Vector3(transform.position.x,transform.position.y,-2f-Random.value);
			Instantiate(goBad);
			spawnedBad = true;
		}
		if (dBad > 1f) {
			spawnedBad = false;
			dBad = 0f;
		}
		if (f > Mathf.PI * 2)
			f = 0f;
		if (GameObject.Find("Center").GetComponent<CenterScr>().alive && ((!spawned || (n==0)) && n < nmax)) {
			go.transform.position = transform.position;
			if (n==0){
				deadTimer+=Time.deltaTime;
				if (deadTimer > k){
					Instantiate (go);
					n++;
					spawned = true;	
					k = Random.value*5f;
					deadTimer = 0f;
				}				
			} else {
				Instantiate (go);
				spawned = true;				
				n++;
			}
		}
		if (d > Mathf.PI / 6 && (Random.value*1000f > 990f)) {
			spawned = false;
			d = 0f;
		}
		if (GameObject.Find("Text").GetComponent<ScoreScr>().score == 0)
			k = 0f;
	
	}
}
