﻿using UnityEngine;

public class CoinSpawnerScr : MonoBehaviour {
	float timer,posTimer,spawnChance;
	public GameObject coin;
	// Use this for initialization
	void Start () {
		spawnChance = 20f;
		timer = 0f;
		posTimer = 0f;
	}
	void SpawnOne(){
		float alpha = Random.value*2*Mathf.PI;
		for (int i=-2; i<3; i++) {
			coin.transform.position = new Vector3 (i*Mathf.Cos(alpha)+transform.position.x,i*Mathf.Sin(alpha)+transform.position.y,1f);
			Instantiate(coin);
		}
	}
	void SpawnTwo(){
		float alpha = Random.value*2*Mathf.PI;
		for (int i=0; i<5; i++) {
			coin.transform.position = new Vector3 (1.5f*Mathf.Cos((2*Mathf.PI*i/5) + alpha)+transform.position.x,1.5f*Mathf.Sin((2*Mathf.PI*i/5) + alpha)+transform.position.y,1f);
			Instantiate(coin);
		}
	}
	// Update is called once per frame
	void Update () {
		if ((GameObject.Find ("Text").GetComponent<ScoreScr> ().score) * 0.0005f <= 30f)
			spawnChance = 20f + (GameObject.Find ("Text").GetComponent<ScoreScr> ().score) * 0.0005f;
		else
			spawnChance = 50f;
		//Debug.Log (spawnChance);
		timer += Time.deltaTime;
		posTimer += Time.deltaTime;
		if (timer > 2f) {
			timer = 0f;
			if (Random.value*100f < spawnChance){
				if (Random.value*100f >= 50f)
					SpawnOne();
				else
					SpawnTwo();
			}
		}	
		if (posTimer >= 0.5f) {
			posTimer = 0f;
			transform.position = new Vector2(Random.value*10f-5f,Random.value*6f-3f);
		}
	}
}
