﻿using UnityEngine;
using System.Collections;

public class ShopButtunScr : MonoBehaviour {
	float timer;
	bool waited = true;
	void OnMouseDown()
	{
		timer = 0f;
		waited = false;
		GetComponent<SpriteRenderer>().color = Color.gray;
	}
	void Wait(float time)
	{
		timer += Time.unscaledDeltaTime;
		if (timer >= time) {
			GetComponent<SpriteRenderer>().color = Color.white;
			GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(2);
		}
	}
	void Update(){
		if (!waited)
			Wait(0.15f);
		if(Application.loadedLevel == 1)
		if (!(GameObject.Find ("Pause").GetComponent<PauseScr> ().paused ||
		   !GameObject.Find ("Center").GetComponent<CenterScr> ().alive) ||
		   (!GameObject.Find ("Center").GetComponent<CenterScr> ().alive &&
			                                   transform.position.y == 0f)){
			Destroy(gameObject);
		}
	}
}
