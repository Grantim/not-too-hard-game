﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyTextScr : MonoBehaviour {
	Text text;
	float scaleTimer;
	public float timerEtalon;
	public bool gotCoin, scaled;
	public int money,scale;
	public Color color;
	// Use this for initialization
	void Start () {
		scale = 1; 
		//
		timerEtalon = 7f + PlayerPrefs.GetInt("coinscaleLvl",0);
		//
		scaleTimer = 0f;
		scaled = false;
		text = GetComponent<Text> ();
		money = PlayerPrefs.GetInt ("money", 0);			
		text.text = "Money: " + money.ToString() + "$";
	}
	
	// Update is called once per frame
	void Update () {
		if (scaled) {
			scale*=2;
			scaled = false;
			scaleTimer = timerEtalon;
			text.color = Color.red;
		}
		{
			if (scaleTimer > 0f) {
				scaleTimer -= Time.deltaTime;
				color = new Color(1f,1f-(scaleTimer/7f),1f-(scaleTimer/7f));
				text.color = color;
			} else {
				scaleTimer = 0f;
				color = Color.white;
				text.color = color;
				scale = 1;
			}
		}
		if (gotCoin) {
			money+=scale;
			text.text = "Money: " + money.ToString() + "$";
			PlayerPrefs.SetInt("money",money);
			gotCoin = false;
		}	
	}
}
