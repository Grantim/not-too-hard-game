﻿using UnityEngine;
using AppodealAds.Unity.Api;

public class CenterScr : MonoBehaviour {
	public Vector3 pos;
	public int multy;
	public bool prs, alive,EVENT1,sheilded,magnetOn,ad,adVideo;
	public float omega = 0f, deathTimer,etalonSheildTimer,etalonMagnetTimer;
	public float sheildTimer,magnetTimer;
	GameObject[] gos;
    public AudioClip[] auClip = new AudioClip[3];
	GameObject sheild,magnet;
	Rigidbody2D rg;
	int i,adCount;
	void Start () {
		adCount = 0;
		ad = false;
        adVideo = false;
		sheild = GameObject.Find ("Sheild");
		magnet = GameObject.Find ("Magnet");
		//
		etalonSheildTimer = 7f + PlayerPrefs.GetInt("sheildLvl",0);
		etalonMagnetTimer = 7f + PlayerPrefs.GetInt("magnetLvl",0);
		//
		sheildTimer = 0f;
		sheilded = false;
		deathTimer = 1.5f;
		EVENT1 = false;
		multy = 0;
		rg = GetComponent<Rigidbody2D> ();
		alive = true;
		prs = Input.GetMouseButton (0);	
	}
	public void Die(){
		adCount++;
        if ((adCount >= 3 || ad) && !adVideo)
        {
            if (Appodeal.isLoaded(Appodeal.INTERSTITIAL))
                Appodeal.show(Appodeal.INTERSTITIAL);
            adCount = 0;
            ad = false;
        }
        else if (adVideo) {
            if (Appodeal.isLoaded(Appodeal.VIDEO))
                Appodeal.show(Appodeal.VIDEO);
            adVideo = false;
            adCount = 0;
        }
	}
	void rebuildcirlce(){
		gos = GameObject.FindGameObjectsWithTag("Circle");
		GameObject.Find ("Spawner").GetComponent<SpawnerScr> ().n = gos.Length;
		multy = 0;
		for (int i=0;i<gos.Length;i++) {
			if (gos[i].GetComponent<FellowScr>().RO < 3f){	
				gos[i].GetComponent<FellowScr>().id = multy;
				multy++;
			}
		}
	}
	// Update is called once per frame
	void Update () {
		if (sheilded){
			sheildTimer+=Time.deltaTime;
			if (sheildTimer <= etalonSheildTimer-1.5f)
				sheild.GetComponent<SpriteRenderer>().color = Color.white;
			else if (sheildTimer < etalonSheildTimer)
				sheild.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.4f);
			else {
				sheilded = false;
				sheildTimer = 0f;
				sheild.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
			}
		}
		if (magnetOn){
			magnetTimer+=Time.deltaTime;
			if (magnetTimer <= etalonMagnetTimer-1.5f)
				magnet.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.8f);
			else if (magnetTimer < etalonMagnetTimer)
				magnet.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0.4f);
			else {
				magnetOn = false;
				magnet.GetComponent<MagnetScr>().magneting = false;
				magnet.GetComponent<MagnetScr>().StopMagneting();
				magnetTimer = 0f;
			}
		}
		if (EVENT1 && prs && alive) {
			rebuildcirlce();
			EVENT1 = false;
		}
		pos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		if (Input.GetMouseButtonDown (0)) {
			//Debug.Log(pos);
			if (Time.timeScale == 1 && ((alive || !((pos.x > -3.5f && pos.x < -0.5f && pos.y > -3.8f && pos.y < -0.8f) || 
                                         (pos.x > 0.5f && pos.x < 3.5f && pos.y > -3.8f && pos.y < -0.8f)))))
				prs = true;
			if (deathTimer <= 0f && !((pos.x > -3.5f && pos.x <-0.5f && pos.y > -3.8f && pos.y < -0.8f)||(pos.x > 0.5f && pos.x <3.5f && pos.y > -3.8f && pos.y < -0.8f))){
				alive = true;
				deathTimer = 1.5f;		
			}
			if (alive){
				if (Time.timeScale == 1)
				GetComponent<SpriteRenderer> ().color = Color.green;				
				GetComponent<Rigidbody2D>().drag = 0f;
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			EVENT1 = true;
			prs = false;
			if (alive)
			GetComponent<SpriteRenderer> ().color = Color.blue;
			multy = 0;
		}
		if (prs && alive) 
			rg.velocity = new Vector2(2f*(pos.x-transform.position.x),2f*(pos.y-transform.position.y));
		omega += Time.deltaTime/2f;
		if (!prs) {
			GetComponent<Rigidbody2D>().drag = 2f;
			if (!alive){
				if (transform.position == new Vector3(0f,0f,-3f))
					GetComponent<Rigidbody2D>().velocity = Vector2.zero;
				else 
					GetComponent<Rigidbody2D>().velocity = new Vector2(-transform.position.x,-transform.position.y);
			}
		}
		if (!alive) {
			deathTimer -= Time.deltaTime;
			sheilded = false;
			magnetOn = false;
			magnet.GetComponent<MagnetScr>().magneting = false;
			magnet.GetComponent<MagnetScr>().StopMagneting();
		}
	}
}
