﻿using UnityEngine;

public class FilterScr : MonoBehaviour {
	public GameObject shop,exit;
	// Use this for initialization
	// Update is called once per frame
	public void Filter(){
		if (GameObject.Find ("Pause").GetComponent<PauseScr> ().paused ||
		    !GameObject.Find ("Center").GetComponent<CenterScr> ().alive) {
			GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, 0.35f);
			shop.transform.position = new Vector3(-2f,0f,transform.position.z-0.6f);
			exit.transform.position = new Vector3(2f,0f,transform.position.z-0.6f);
			GameObject.Find("GameOver").GetComponent<SpriteRenderer>().color = new Color (0, 0, 0, 0);
			if (!GameObject.Find ("Center").GetComponent<CenterScr> ().alive) {
                if (PlayerPrefs.GetInt("sound", 1) == 1)
                {
                    GameObject.Find("Center").GetComponent<AudioSource>().clip =
                        GameObject.Find("Center").GetComponent<CenterScr>().auClip[1];
                    GameObject.Find("Center").GetComponent<AudioSource>().Play();
                }
                shop.transform.position = new Vector3(-2f,-2.3f,transform.position.z-0.6f);
				exit.transform.position = new Vector3(2f,-2.3f,transform.position.z-0.6f);
				GameObject.Find("GameOver").GetComponent<SpriteRenderer>().color = Color.white;
			}
			Instantiate(shop);
			Instantiate(exit);
		}
	}
	void Update () {
		if (!(GameObject.Find ("Pause").GetComponent<PauseScr> ().paused ||
		      !GameObject.Find ("Center").GetComponent<CenterScr> ().alive)){
			GetComponent<SpriteRenderer> ().color = new Color (0, 0, 0, 0);
			GameObject.Find("GameOver").GetComponent<SpriteRenderer>().color = new Color (0, 0, 0, 0);
		}
	
	}
}
