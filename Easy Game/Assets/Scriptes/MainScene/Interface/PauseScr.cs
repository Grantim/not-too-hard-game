﻿using UnityEngine;

public class PauseScr : MonoBehaviour {
	public Sprite play,pause;
	public bool paused;
    bool alowed = true;
	// Use this for initialization
	void Start(){
		paused = false;		
		Time.timeScale = 1;
	}
    void OnApplicationFocus(bool focus)
    {
        if (Time.timeScale == 1f)
        {
            if (GameObject.Find("Center").GetComponent<CenterScr>().alive)
            {
                Time.timeScale = 0f;
                paused = true;
                GetComponent<SpriteRenderer>().sprite = play;
                GameObject.Find("Filter").GetComponent<FilterScr>().Filter();
                GetComponent<BoxCollider2D>().size = new Vector2(35f, 25f);
                GetComponent<BoxCollider2D>().offset = new Vector2(-13f, -10f);
            }
        }
    }
	void OnMouseDown(){
		if (Time.timeScale == 1f) {
			if (GameObject.Find ("Center").GetComponent<CenterScr> ().alive) {
				Time.timeScale = 0f;
				paused = true;
				GetComponent<SpriteRenderer> ().sprite = play;
				GameObject.Find ("Filter").GetComponent<FilterScr> ().Filter ();	
				GetComponent<BoxCollider2D>().size = new Vector2(35f,25f);
				GetComponent<BoxCollider2D>().offset = new Vector2(-13f,-10f);
			}
		} else {
			Time.timeScale = 1;				
			paused = false;				
			GetComponent<SpriteRenderer>().sprite = pause;	
			GetComponent<BoxCollider2D>().size = new Vector2(1.5f,1.5f);
			GetComponent<BoxCollider2D>().offset = new Vector2(0.2f,0.2f);
		}
	}
    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) && Time.timeScale == 0f)
            OnMouseDown();
        else if (Input.GetKeyDown(KeyCode.Escape) && alowed) {
            alowed = false;
            GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(0);
        }
    }
}
