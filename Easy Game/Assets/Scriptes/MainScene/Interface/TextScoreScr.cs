﻿using UnityEngine;

public class TextScoreScr : MonoBehaviour {
	Vector3 pos;
	float clr = 1f;
	Color color;
	// Use this for initialization
	void Start () {
		pos = Camera.main.WorldToViewportPoint (transform.position);
		color = GetComponent<GUIText> ().color;
//		Debug.Log (pos);
		pos.z = -7f;
	}
	
	// Update is called once per frame
	void Update () {
		pos.y += 0.002f;
		clr -= 0.01f;
		transform.position = pos;
		if (GetComponent<GUIText> ().color.a > 0)
			GetComponent<GUIText> ().color = new Color(color.r,color.g,color.b,clr);
		else
			Destroy (gameObject);
	}
}
