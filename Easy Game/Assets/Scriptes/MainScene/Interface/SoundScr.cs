﻿using UnityEngine;
using System.Collections;

public class SoundScr : MonoBehaviour {
    public Sprite soundOn, soundOff;
    // Use this for initialization
    void Start() {
        if (PlayerPrefs.GetInt("sound", 1) == 1)
            GetComponent<SpriteRenderer>().sprite = soundOn;
        else
            GetComponent<SpriteRenderer>().sprite = soundOff;
    }

    void OnMouseDown() {
        if (PlayerPrefs.GetInt("sound", 1) == 1)
        {
            PlayerPrefs.SetInt("sound", 0);
            GetComponent<SpriteRenderer>().sprite = soundOff;
        }
        else {
            PlayerPrefs.SetInt("sound", 1);
            GetComponent<SpriteRenderer>().sprite = soundOn;
        }
    }
}
