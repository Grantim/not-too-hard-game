﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreScr : MonoBehaviour {
	Text text;
	public int score,k,highscore;
	float timer,timer2;
	// Use this for initialization
	void Start () {
		//Debug.Log (Screen.height);
		text = GetComponent<Text> ();
		highscore = PlayerPrefs.GetInt("highscore",0);
		score = 0;
		timer = 0;
		timer2 = 0;
		k = 1;
	}

	// Update is called once per frame
	void Update () {
        if (score >= 10000 && score < 20000)
        {
            GameObject.Find("Center").GetComponent<CenterScr>().ad = true;
        }
        else if (score >= 20000)
        {
            GameObject.Find("Center").GetComponent<CenterScr>().ad = false;
            GameObject.Find("Center").GetComponent<CenterScr>().adVideo = true;
        }
		if ((GameObject.Find ("Center").GetComponent<CenterScr> ().ad || GameObject.Find("Center").GetComponent<CenterScr>().adVideo) && score == 0)
			GameObject.Find ("Center").GetComponent<CenterScr> ().Die ();
		if (score > highscore) {
			highscore = score;
			PlayerPrefs.SetInt ("highscore", highscore);
		}
		timer += Time.deltaTime;
		if (!GameObject.Find ("Center").GetComponent<CenterScr> ().alive)
			score = 0;
		else if (GameObject.Find ("Center").GetComponent<CenterScr> ().prs)
		if (timer > 0.2f) {
			if (GameObject.Find("Center").GetComponent<CenterScr>().multy!=0)
			k = 1;
			timer = 0f;
			score += GameObject.Find("Center").GetComponent<CenterScr>().multy;
		}
		if (GameObject.Find("Center").GetComponent<CenterScr>().multy==0) {
			timer2 += Time.deltaTime;
			if (score > 0 && timer2 > 0.2f) {
				timer2 = 0;
				if (score - k > 0){				
					score -= k;				
					k += 5;
				}
				else
					score = 0;
			} 
		}
		if (GameObject.Find ("Center").GetComponent<CenterScr> ().alive) {
			text.text = "Score: " + score.ToString () + "  x" + GameObject.Find ("Center").GetComponent<CenterScr> ().multy.ToString () + "\nHighScore: " + highscore.ToString ();
		}
	}
}
