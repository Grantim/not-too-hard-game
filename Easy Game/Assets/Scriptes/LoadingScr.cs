﻿using UnityEngine;
using System.Collections;

public class LoadingScr : MonoBehaviour {
	int Lvl;
	public bool hiden,hide,perform,performed;
	// Use this for initialization
	void Start () {
		hide = performed;	
	}
	public void Loadlvl(int lvl){
		perform = true;
		Lvl = lvl;
	}
	// Update is called once per frame
	void Update () {
		if (transform.position.x >= 0f && perform) {
			transform.position = new Vector3(0f,0f,-9.7f);
			perform = false;
			performed = true;
			hiden = false;
			Application.LoadLevel(Lvl);
		}
		if (transform.position.x >= 20f && hide) {
			transform.position = new Vector3(-20f,0f,-9.7f);
			hide = false;
			performed = false;
			hiden = true;
		}
		if (performed && hide) {
			transform.position += new Vector3(0.3f,0f,0f);
		}
		if (hiden && perform) {			
			transform.position += new Vector3(0.3f,0f,0f);
		}
		if (Application.loadedLevel == 0 && perform) {
			GameObject.Find("tutor").GetComponent<GUIText>().color = new Color(1f,1f,1f,0f);
		}	
	}
}
