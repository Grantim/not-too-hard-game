﻿using UnityEngine;
using System.Collections;

public class BGStartScr : MonoBehaviour {
    float x, y,timer,timer2;
	// Use this for initialization
	void Start () {
        timer = 0f;
        timer2 = 0f;
	}
	
	// Update is called once per frame
	void Update () {
        if (GameObject.Find("StartButton").GetComponent<StartButton>().r >= 0.5f)
            timer += Time.deltaTime;
        else
            timer -= Time.deltaTime;
        if (timer >= 6.29f)
            timer = 0f;
        x = 0.5f*Mathf.Sin(timer);
        y = 0.5f*Mathf.Cos(timer);
        transform.position = new Vector3(x/ transform.position.z, y/transform.position.z, transform.position.z);
	
	}
}
