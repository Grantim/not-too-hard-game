﻿using UnityEngine;
using AppodealAds.Unity.Api;
using System.Collections;

public class StartButton : MonoBehaviour
{
	float timer, timer2;
	public float r;
	bool waited = true;
	void Start() {
		r = Random.value;		
		Appodeal.initialize ("c7ad83e154d8dbd1309d275c6ff15f3af7f067d21accb321",Appodeal.INTERSTITIAL | Appodeal.VIDEO);
	}
	void OnMouseDown()
	{
		timer = 0f;
		waited = false;
		GetComponent<SpriteRenderer>().color = Color.gray;
	}
	void Wait(float time)
	{
		timer += Time.unscaledDeltaTime;
		if (timer >= time) {
			GetComponent<SpriteRenderer>().color = Color.white;
			if (PlayerPrefs.GetInt("tutor", 0) == 1)
				GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(1);
			else
				GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(3);
		}
	}
	void Update()
	{
		timer2 += Time.deltaTime;
		if (timer2 > 1f) {
			r = Random.value;
			timer2 = 0f;
		}
		if (!waited)
			Wait(0.15f);
	}
}
