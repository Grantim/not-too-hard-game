﻿using UnityEngine;
using System.Collections;

public class LiftScr : MonoBehaviour {
	float max = 4.5f, min = -7f,newY, mpos, timer, start,k,scaleY;
	public float delta;
	bool presed,chek,push,move,done;
	Vector2 mousePosition;
	// Use this for initialization
	void Start () {
		//Debug.Log (Camera.main.ScreenToWorldPoint (GameObject.Find ("Money").transform.position).x);
		transform.position = new Vector3 (Camera.main.ScreenToWorldPoint(GameObject.Find("Money").transform.position).x+9.072532f,transform.position.y,transform.position.z);
		timer = 0f;
		newY = 0f;
		mpos = 0f;
		delta = 0f;
		start = 0f;
		chek = false;
		delta = 0f;
		move = false;
		push = false;
		Time.timeScale = 1f;
	}
	void OnMouseDown(){
		start = transform.position.y;
		//move = true;
		timer = 0f;
		delta = 0f;
		presed = true;
		mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		scaleY = mousePosition.y;
		newY = mousePosition.y;
		done = false;
	}
	void OnMouseUp(){
		move = false;
		if (!push) {
			push = true;
			delta = (Camera.main.ScreenToWorldPoint (Input.mousePosition).y - mousePosition.y)/(5f*timer);
			if (delta > 10f){
				delta = 10f;
			}
			if (delta < -10f)
				delta = -10f;
		} else
			delta = 0f;
		presed = false;
	}
	void ScaleZero(){
		transform.localScale = new Vector3 (1f, 1f, 1f);
		push = false;
		chek = false;

	}
	// Update is called once per frame
	void Update () {
		if (presed) {
			mpos = Camera.main.ScreenToWorldPoint (Input.mousePosition).y;
			if (Mathf.Abs (-mousePosition.y + mpos) >= 0.5f){
				if (mpos - mousePosition.y > 0f && !done){
					k = -0.5f;
					done =true;
				}
				else if (!done){
					k = 0.5f;
					done =true;
				}
				move = true;
			}
			timer+=Time.deltaTime;
		}
		else
			mpos = delta;
		if (transform.localScale.y > 1f)
			transform.localScale = new Vector3 (1f,1f,1f); 
		if (move && 
		    presed && 
		    (transform.position.y <= max && transform.position.y >= min)) {
			push = false;
			if (transform.localScale.y < 1f) {
				transform.localScale += new Vector3 (0f, 0.015f, 0f);
			}
			newY = (Camera.main.ScreenToWorldPoint (Input.mousePosition)).y;
			if (transform.position.y < max && transform.position.y > min)
				scaleY = newY;
			transform.position = new Vector3(transform.position.x,
			                                 start + k + (-mousePosition.y + newY),
			                                 transform.position.z);
		}  
		if (transform.position.y > max)
			transform.position = new Vector3 (transform.position.x,max,transform.position.z);
		if (transform.position.y < min)
			transform.position = new Vector3 (transform.position.x,min,transform.position.z);			
		if (transform.position.y == max &&
		    move &&
		    presed && 
		    mpos > scaleY &&
		    (transform.localScale.y > 0.87f)){
			push = true;
			//newY = (Camera.main.ScreenToWorldPoint (Input.mousePosition)).y;
			transform.localScale = new Vector3(1f, 1f - (-scaleY + mpos)/25f,1f);
		}
		if (transform.position.y == min &&
		    move &&
		    presed && 
		    mpos < scaleY &&
		    (transform.localScale.y > 0.87f)){
			push = true;
			//newY = (Camera.main.ScreenToWorldPoint (Input.mousePosition)).y;
			transform.localScale = new Vector3(1f, 1f - (scaleY - mpos)/25f,1f);
		}
		if (transform.localScale.y < 0.87f)
			transform.localScale = new Vector3 (1f,0.87f,1f);





		//////////////////////////////////////
		if ((-mousePosition.y + mpos <= -0.5f) && 
		    !presed && delta < -3f  &&
		    (transform.position.y > min)) {
			delta += 0.05f;
			//push = false;
			if (transform.localScale.y < 1f) {
				transform.localScale += new Vector3 (0f, 0.015f, 0f);
			}
			newY = delta;
			transform.position = new Vector3(transform.position.x,
			                                 transform.position.y + (-mousePosition.y + newY)/25f,
			                                 transform.position.z);
		} 
		/////////////////////
		/// 
		if ((-mousePosition.y + mpos) >= 0.5f && 
		    !presed && delta > 3f &&
		    (transform.position.y < max)) {
			delta -= 0.05f;
			//push = false;
			if (transform.localScale.y < 1f) {
				transform.localScale += new Vector3 (0f, 0.015f, 0f);
			}
			newY = delta;
			transform.position = new Vector3(transform.position.x,
			                                 transform.position.y + (-mousePosition.y + newY)/25f,
			                                 transform.position.z);
		}

		/////////////////////////


		if (!presed && transform.localScale.y < 1f) {
			transform.localScale += new Vector3 (0f, 0.015f, 0f);
			if (!chek)
				chek = true;
		} else if (!presed && chek && push)
			ScaleZero ();
	
	}
}
