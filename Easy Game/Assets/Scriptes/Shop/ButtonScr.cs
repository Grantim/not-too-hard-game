﻿using UnityEngine;
using System.Collections;

public class ButtonScr : MonoBehaviour {
	public string type;
	public Sprite[] priceId = new Sprite[6];
	public int[] price = new int[5];
	GameObject[] points, buttons;
	int upgradeLvl;
	bool Lock,alowed = true;
	// Use this for initialization
	public void chekLock(){
		if (upgradeLvl == 5) {
			Lock = true;
			GetComponent<SpriteRenderer> ().color = Color.gray;
			transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
			                                 transform.position.y,0f);
		} else
		if (PlayerPrefs.GetInt ("money", 0) < price [upgradeLvl]) {
			Lock = true;
			GetComponent<SpriteRenderer> ().color = Color.gray;
			transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
			                                 transform.position.y,0f);
		} else {
			Lock = false;
			GetComponent<SpriteRenderer> ().color = Color.white;
			transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
			                                 transform.position.y,-8.5f);
		}
	}
	void Start () {
		points = GameObject.FindGameObjectsWithTag ("Point");
		buttons = GameObject.FindGameObjectsWithTag ("Button");
		transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
		                                 transform.position.y,-8.5f);
		if (PlayerPrefs.GetInt ("godLvl", 0) == 5) {
			PlayerPrefs.SetInt("dropLvl",5);
			PlayerPrefs.SetInt("sheildLvl",5);
			PlayerPrefs.SetInt("magnetLvl",5);
			PlayerPrefs.SetInt("coinscaleLvl",5);
		}
		upgradeLvl = PlayerPrefs.GetInt(type + "Lvl",0);
		GetComponent<SpriteRenderer>().sprite = priceId[upgradeLvl];	
		chekLock ();
	}
	void OnMouseDown (){
		GetComponent<SpriteRenderer> ().color = Color.gray;
	}
	void OnMouseUp(){
		if (GetComponent<SpriteRenderer> ().color == Color.gray)
		if (!Lock){
			foreach(GameObject point in points)
				point.GetComponent<UpgradePointScr>().Buy(type,upgradeLvl);
			GameObject.Find ("Money").GetComponent<MoneyShopScr>().Spend(price[upgradeLvl]);
			upgradeLvl++;
			if (upgradeLvl==5 && type=="god"){
				foreach(GameObject button in buttons){
					PlayerPrefs.SetInt(button.GetComponent<ButtonScr>().type + "Lvl",upgradeLvl);
					button.GetComponent<ButtonScr>().upgradeLvl = upgradeLvl;
					button.GetComponent<SpriteRenderer>().sprite = priceId[upgradeLvl];
					button.GetComponent<SpriteRenderer> ().color = Color.white;
					button.GetComponent<ButtonScr>().chekLock();
				}
			} else {
				PlayerPrefs.SetInt(type + "Lvl",upgradeLvl);
				GetComponent<SpriteRenderer>().sprite = priceId[upgradeLvl];
				GetComponent<SpriteRenderer> ().color = Color.white;
			}
		}		
		chekLock ();
	}
	void OnMouseExit(){
		if (!Lock)
			GetComponent<SpriteRenderer> ().color = Color.white;
	}
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) && alowed)
        {
            alowed = false;
            GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(0);
        }
        if (transform.position.x != Camera.main.ViewportToWorldPoint (new Vector3 (0.85f, 0f)).x) {
			if (Lock)
				transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
				                                 transform.position.y,0f);
			else
				transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.85f,0f)).x,
				                                 transform.position.y,-8.5f);
		}
	}
}
