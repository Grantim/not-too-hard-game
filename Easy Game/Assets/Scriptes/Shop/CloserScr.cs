﻿using UnityEngine;
using System.Collections;

public class CloserScr : MonoBehaviour {
    Vector3 pos;
	// Use this for initialization
	void Start () {
        pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(pos.x-(GameObject.Find("Lift").transform.position.x) / ((transform.position.z+9.6f)*150f),
                                 pos.y-(GameObject.Find("Lift").transform.position.y) / ((transform.position.z+9.6f)*150f),
                                 transform.position.z);

    }
}
