﻿using UnityEngine;
using System.Collections;

public class UpgradePointScr : MonoBehaviour {
	public string type;
	public int id;

	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetInt(type + "Lvl", 0) >= id)
			GetComponent<Animator> ().SetBool ("DONE",true);	
	}
	
	public void Buy(string T, int i){
		if ((T == type && i == id - 1) || (T == "god" && i == 4))
			GetComponent<Animator> ().SetBool ("ON",true);
	}
}
