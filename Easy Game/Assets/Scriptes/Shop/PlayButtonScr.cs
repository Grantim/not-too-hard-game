﻿using UnityEngine;
using UnityEngine.UI;

public class PlayButtonScr : MonoBehaviour {
	Text text;
	void Start(){
		text = GetComponent<Text> ();		
		text.fontSize = (Camera.main.pixelHeight / 13);
	}
	float timer;
	bool waited = true;
	void OnMouseDown()
	{
		timer = 0f;
		waited = false;
		GetComponent<Text>().color = Color.gray;
	}
	void Wait(float time)
	{
		timer += Time.unscaledDeltaTime;
		if (timer >= time) {
			GetComponent<Text>().color = Color.white;
			GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(1);
		}
	}
	void Update()
	{
		if (!waited)
			Wait(0.15f);
	}
}
