﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyShopScr : MonoBehaviour {
	public int money;
	Text text;
	GameObject[] buttons;
	// Use this for initialization
	void Start () {		
		buttons = GameObject.FindGameObjectsWithTag ("Button");
		text = GetComponent<Text> ();
		money = PlayerPrefs.GetInt ("money", 0);
		text.text = "Money: " + money.ToString () + "$";
		text.fontSize = (Camera.main.pixelHeight / 13);

	}
	public void Spend(int baks){
		money -= baks;
		PlayerPrefs.SetInt("money",money);		
		text.text = "Money: " + money.ToString () + "$";
		foreach (GameObject button in buttons) {
			button.GetComponent<ButtonScr>().chekLock();
		}
	}
}
