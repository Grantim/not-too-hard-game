﻿using UnityEngine;
using System.Collections;

public class SexyLiftScr : MonoBehaviour {
	bool prs;
	public float max = 1.5f, min = -3.6f,y;
	// Use this for initialization
	void Start () {
		transform.position = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(0.97f,0f)).x,
		                                                transform.position.y,-8.3f);
		GameObject.Find ("LiftBox").transform.position = new Vector3 (Camera.main.ViewportToWorldPoint(new Vector3(0.97f,0f)).x,-1.2f,-8.29f);
		prs = false;	
		y = max;
	}
	void OnMouseDown(){
		prs = true;
		GameObject.Find ("Lift").GetComponent<LiftScr> ().delta = 0f;
		y = -Camera.main.ScreenToWorldPoint(Input.mousePosition).y + transform.position.y;
	}
	void OnMouseUp(){
		prs = false;
	}
	// Update is called once per frame
	void Update () {
		if (!prs) {
			y = -((max - min) / 11.5f) * (GameObject.Find ("Lift").transform.position.y + 7f) + max;
			transform.position = new Vector3 (transform.position.x, y, -8.3f);
		} else {
			if (y + Camera.main.ScreenToWorldPoint(Input.mousePosition).y < max && y + Camera.main.ScreenToWorldPoint(Input.mousePosition).y >min){
				transform.position = new Vector3 (transform.position.x,y + Camera.main.ScreenToWorldPoint(Input.mousePosition).y, -8.3f);
				GameObject.Find ("Lift").transform.position = new Vector3(GameObject.Find ("Lift").transform.position.x,
				                                                          -(11.5f/(max - min))*(transform.position.y - max) - 7f,
				                                                          GameObject.Find ("Lift").transform.position.z);
			}
		}	
		if (transform.position.y>max)
			transform.position = new Vector3 (transform.position.x,max, -8.3f);
		if (transform.position.y<min)
			transform.position = new Vector3 (transform.position.x,min, -8.3f);
	}
}
