﻿using UnityEngine;
using System.Collections;

public class TutorScr : MonoBehaviour {
	//public int id;
	bool moveOut, moveTo;
	void Start(){
		moveTo = false;
		moveOut = false;
	}
	// Use this for initialization
	public void MoveOut (){
		moveOut = true;
	}
	public void MoveTo(){
		moveTo = true;
	}
	// Update is called once per frame
	void Update () {
		if (moveOut && transform.position.x > -20f)
			transform.position -= new Vector3 (0.2f,0f,0f);
		if (moveTo && transform.position.x > 0f) {
			transform.position -= new Vector3 (0.2f,0f,0f);
			GameObject.Find ("Text").GetComponent<NextButtun> ().alowed = false;
		}
		if (moveTo && transform.position.x < 0f) {
			transform.position = Vector3.zero;
			GameObject.Find("Text").GetComponent<NextButtun>().alowed = true;
			moveTo = false;
		}
	
	}
}
