﻿using UnityEngine;

public class HelpScr : MonoBehaviour {
	float timer;
	bool waited = true;
	void Start(){
		GetComponent<GUIText> ().color = new Color (1f,1f,1f,1f);
	}
	void OnMouseDown()
	{
		timer = 0f;
		waited = false;
		GetComponent<GUIText>().color = Color.gray;
	}
	void Wait(float time)
	{
		timer += Time.unscaledDeltaTime;
		if (timer >= time) {
			GetComponent<GUIText> ().color = new Color (1f,1f,1f,0f);
			GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(3);
		}
	}
	void Update()
	{
		if (!waited)
			Wait(0.15f);
	}
}