﻿using UnityEngine;
using UnityEngine.UI;

public class NextButtun : MonoBehaviour {
	public bool alowed;
	int id;
	Text text;
	void Start(){
		alowed = true;
		id = 0;
		text = GetComponent<Text> ();		
		text.fontSize = (Camera.main.pixelHeight / 13);
	}
	float timer;
	bool waited = true;
	void OnMouseDown()
	{
		timer = 0f;
		waited = false;
		GetComponent<Text>().color = Color.gray;
	}
	void Wait(float time)
	{
		timer += Time.unscaledDeltaTime;
		if (timer >= time) {
			GetComponent<Text>().color = Color.white;
			waited = true;
			if (alowed){
				if (id == 0)
				{
					id++;
					GameObject.Find("tutor2").GetComponent<TutorScr>().MoveOut();
					GameObject.Find("tutor3").GetComponent<TutorScr>().MoveTo();
				} else
				if (id == 1)
				{
					id++;
					GameObject.Find("tutor3").GetComponent<TutorScr>().MoveOut();
					GameObject.Find("tutor1").GetComponent<TutorScr>().MoveTo();
				} else
				if (id == 2)
				{
					GameObject.Find("tutor1").GetComponent<TutorScr>().MoveOut();
					GameObject.Find("tutor4").GetComponent<TutorScr>().MoveTo();
					text.text = "play";
					id ++;
				} else
				if (id == 3){
					PlayerPrefs.SetInt("tutor",1);
					GameObject.Find("Loading").GetComponent<LoadingScr>().Loadlvl(1);
				}
			}
		}
	}
	void Update()
	{
		if (!waited)
			Wait(0.15f);
	}
}
